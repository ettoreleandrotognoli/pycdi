# -*- encoding: utf-8 -*-
from .core import Inject, Producer
from .utils import Singleton
